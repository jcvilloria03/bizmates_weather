<?php

namespace App\Http\Controllers;

use App\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class MapsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function show()
    {
        return view('map');
    }

    public function index(Request $request)
    {
        $weatherInfo = [];
        $request = request();

        $appid = 'bd31ced6f03605108209d424cabb1bde';
        $units = 'metric';
        $city = $request->city;
        $countrycode = 'JP';

        $json = file_get_contents(
            "http://api.openweathermap.org/data/2.5/weather?q=" . $city . "," . $countrycode . "&units=" . $units . "&appid=" . $appid . ""
        );

        $forc = file_get_contents(
            "http://api.openweathermap.org/data/2.5/forecast?q=" . $city . "," . $countrycode . "&appid=" . $appid . ""
        );
        $winfo = json_decode($json);
        $forecastinfo = json_decode($forc);

        $combi = $winfo->timezone + $winfo->dt;
        $winfo->currentDate = date('Y-m-d H:i', $combi);
        $winfo->weather[0]->description = ucwords($winfo->weather[0]->description);
        $weatherInfo['weather'] = $winfo;

        foreach ($forecastinfo->list as $forecastinfoListItem) {
            $forecastinfoListItem->day = date('l', $forecastinfoListItem->dt);
            $forecastinfoListItem->weather[0]->description = ucwords($forecastinfoListItem->weather[0]->description);
        }

        $weatherInfo['forecast'] = $forecastinfo;

        return $weatherInfo;
    }
}
